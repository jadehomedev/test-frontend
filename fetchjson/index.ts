import axios from 'axios';
// fake json url
const url = 'http://jsonplaceholder.typicode.com/todos/1';

interface Todo {
    id: number;
    title: string;
    completed: boolean;
}

axios.get(url).then(response => {
    //response.data has properties of id, title and completed
    const todo = response.data as Todo;
    const id = todo.id;
    const title = todo.title;
    const completed = todo.completed;

    logTodo(id, title, completed);
});

//error function
const logTodo = (id: number, title: string, completed: boolean) => {
  console.log(`
    Todo with ID: ${id}
    A title of : ${title}
    Is it complete? ${completed}
    `); 
};

// run in terminal: [nhkum@xxxxxx fetchjson]$ ts-node index.ts