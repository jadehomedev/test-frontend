class Travel {
    constructor(public company: string) {}
    
    protected selectedName(): void {
      console.log('Test');
    }
   }
    
   const travel = new Travel('ST');
   console.log(travel.company);
    
   //inheritance
   class Destination extends Travel {
    constructor(public saison: number, company: string) {
      super(company);
    }
    
    private product(): void {
      console.log('Nature');
    }

    startTravellingProcess(): void {
      this.product();
      this.selectedName();
    }
   }

   const dest = new Destination(2020, 'ST');
   dest.startTravellingProcess();   