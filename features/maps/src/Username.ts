import faker from 'faker';

class Username {
    name: string;
    location: {
        lat: number;
        lng: number;
    };

    constructor() {
        this.name = faker.name.firstName();
        this.location = {
        // parseFloat 
        lat: parseFloat(faker.address.latitude()),
        lng: parseFloat(faker.address.longitude())
        };
    }
}