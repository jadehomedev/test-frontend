//Functions in Interfaces
interface Reuseable {
    info(): string;
}

const travelInfo = {
    name: 'ST',
    date: new Date(),
    booked : true,
    info(): string {
        return `Name: ${this.name}`;
    }
};

const printTravel = (product: Reuseable): void => {
    console.log(product.info());
};

printTravel(travelInfo);


